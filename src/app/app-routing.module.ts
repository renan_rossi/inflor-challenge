import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './modules/pages/main/main.component';
import { IflClockinComponent } from './shared/components/ifl-clockin/ifl-clockin.component';
import { IflEmployeeAdministrationComponent } from './shared/components/ifl-employee-administration/ifl-employee-administration.component';
import { IflEmployeeRegisterComponent } from './shared/components/ifl-employee-register/ifl-employee-register.component';
import { IflClockinReportsComponent } from './shared/components/ifl-clockin-reports/ifl-clockin-reports.component';


const routes: Routes = [
  { path: '', redirectTo: '/ponto', pathMatch: 'full' },
  {
    path: 'ponto', component: MainComponent,
    children: [
      {
      path: 'registro',
      component: IflClockinComponent
    },
    {
      path: 'relatorio',
      component: IflClockinReportsComponent
    },
    {
      path: 'colaboradores',
      component: IflEmployeeAdministrationComponent,
    },
    {
      path: 'colaboradores/cadastro',
      component: IflEmployeeRegisterComponent
    }
  ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
