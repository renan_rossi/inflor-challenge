import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from 'src/app/shared/models/entities/employee';
import Workday from 'src/app/shared/models/entities/workday';
import Entry from 'src/app/shared/models/entities/entry';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class WorkersService {

  constructor(private http: HttpClient) {
    this.mockWorker() //Mocked me :)
  }

  private api = 'workers'
  private seq_workers = 0

  private _workers: Array<Employee> = []
  get workers() { return this._workers }


  /**
   * Lists all registered workers
   */
  async list(): Promise<Array<Employee>> {

    return Promise.resolve(this._workers)
  }

  /**
   * Inserts a new worker.
   * @param worker 
   */
  async insert(worker: Employee): Promise<void> {
    worker.id = ++this.seq_workers
    this._workers.push(worker)

    return Promise.resolve();
  }

  /**
   * Retrieves a worker based on it's id.
   * @param id 
   */
  async retrieve(id: number): Promise<Employee> {
    let worker = this._workers.find(x => x.id == id)

    return Promise.resolve(worker)
  }

  /**
   * Removes a worker from the sistem based on it's id.
   * @param id 
   */
  async delete(id: number): Promise<null> {
    this._workers.splice(this._workers.findIndex(e => e.id == id), 1)

    return Promise.resolve(null)
  }

  private async mockWorker() {
    let worker = new Employee('Renan Rossi Dias', moment('12/03/2019', "DD/MM/YYYY").toDate(), "Frontend Developer", 8)
    await this.insert(worker)
  }

}
