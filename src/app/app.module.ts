import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IflClockinComponent } from './shared/components/ifl-clockin/ifl-clockin.component';
import { IflMainHeaderComponent } from './shared/components/ifl-main-header/ifl-main-header.component';
import { IflMainFooterComponent } from './shared/components/ifl-main-footer/ifl-main-footer.component';
import { MainComponent } from './modules/pages/main/main.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IflEmployeeAdministrationComponent } from './shared/components/ifl-employee-administration/ifl-employee-administration.component';
import { IflEmployeeRegisterComponent } from './shared/components/ifl-employee-register/ifl-employee-register.component';
import { IflClockinReportsComponent } from './shared/components/ifl-clockin-reports/ifl-clockin-reports.component';



@NgModule({
  declarations: [
    AppComponent,
    IflClockinComponent,
    IflMainHeaderComponent,
    IflMainFooterComponent,
    MainComponent,
    IflEmployeeAdministrationComponent,
    IflEmployeeRegisterComponent,
    IflClockinReportsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
