import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IflClockinReportsComponent } from './ifl-clockin-reports.component';

describe('IflClockinReportsComponent', () => {
  let component: IflClockinReportsComponent;
  let fixture: ComponentFixture<IflClockinReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IflClockinReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IflClockinReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
