import { Component, OnInit } from '@angular/core';
import { Employee } from '../../models/entities/employee';
import Workday from '../../models/entities/workday';
import * as moment from 'moment';
import { WorkersService } from 'src/app/core/services/workers.service';

@Component({
  selector: 'app-ifl-clockin-reports',
  templateUrl: './ifl-clockin-reports.component.html',
  styleUrls: ['./ifl-clockin-reports.component.scss']
})
export class IflClockinReportsComponent implements OnInit {

  private workers: Array<Employee>
  private worker: Employee
  private workdays: Array<Workday>
  private months: Array<string>
  private month: number
  private year: number
  private today: moment.Moment
  private id: number
  
  constructor(private workersService: WorkersService) { }

  ngOnInit() {

    this.id = 0
    this.today = moment().locale('pt-br')
    this.worker = new Employee('', new Date())
    this.workdays = new Array<Workday>()
    this.months = this.today.localeData().months()
    this.month = this.today.month()
    this.year = this.today.year()

    this.listWorkers()
  }




  async listWorkers() {
    let workers = await this.workersService.list()

    this.workers = workers;
  }

  async retrieveWorker(id: number) {

    if (id == 0) {
      return
    }

    let worker = await this.workersService.retrieve(id)
    this.worker = worker
  }

  getMonthlyWorkedTimeStr(worker: Employee, month: number, year: number){
    let total = worker.getMonthlyWorkedTimeStr(month, year);

    return total;
  }

  


  retrieveWorkDaysForMonth(month: number, year: number) {
    this.workdays = this.worker.workLog.filter(w => w.date.getMonth() == month && w.date.getFullYear() == year)

  }
}
