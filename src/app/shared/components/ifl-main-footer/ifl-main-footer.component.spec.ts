import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IflMainFooterComponent } from './ifl-main-footer.component';

describe('IflMainFooterComponent', () => {
  let component: IflMainFooterComponent;
  let fixture: ComponentFixture<IflMainFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IflMainFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IflMainFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
