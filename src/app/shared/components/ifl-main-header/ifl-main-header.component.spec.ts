import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IflMainHeaderComponent } from './ifl-main-header.component';

describe('IflMainHeaderComponent', () => {
  let component: IflMainHeaderComponent;
  let fixture: ComponentFixture<IflMainHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IflMainHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IflMainHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
