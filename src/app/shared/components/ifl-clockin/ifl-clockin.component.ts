import { Component, OnInit } from '@angular/core';
import Workday from '../../models/entities/workday';
import Entry from '../../models/entities/entry';
import { Employee } from '../../models/entities/employee';
import * as moment from 'moment';
import { WorkersService } from '../../../core/services/workers.service';

@Component({
  selector: 'ifl-clockin',
  templateUrl: './ifl-clockin.component.html',
  styleUrls: ['./ifl-clockin.component.scss']
})
export class IflClockinComponent implements OnInit {

  constructor(private workersService: WorkersService) { }

  private worker: Employee
  private workers: Array<Employee>
  private workdays: Array<Workday>
  private months: Array<string>
  private month: number
  private year: number
  private today: moment.Moment
  private id: number

  async ngOnInit() {
    this.id = 0
    this.today = moment().locale('pt-br')
    this.worker = new Employee('', new Date())
    this.workdays = new Array<Workday>()
    this.months = this.today.localeData().months()
    this.month = this.today.month()
    this.year = this.today.year()

    this.listWorkers()
  }


  async listWorkers() {
    let workers = await this.workersService.list()

    this.workers = workers
  }

  async retrieveWorker(id: number) {

    if (id == 0) {
      return
    }

    let worker = await this.workersService.retrieve(id)    
    this.worker = worker
    this.retrieveWorkDaysForMonth(this.month, this.year)
  }

  retrieveWorkDaysForMonth(month: number, year: number) {
    this.workdays = this.worker.workLog.filter(w => w.date.getMonth() == month && w.date.getFullYear() == year)

  }

  newEntry(workday: Workday) {
    let entry = new Entry()
    workday.editEntry(entry)  
  }

  removeEntry(workday: Workday, entry: Entry){
    workday.removeEntry(entry)
  }

  saveEntry(workday: Workday, entry: Entry) {
    workday.saveEntry(entry)
  }


}
