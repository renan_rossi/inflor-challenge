import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IflClockinComponent } from './ifl-clockin.component';

describe('IflClockinComponent', () => {
  let component: IflClockinComponent;
  let fixture: ComponentFixture<IflClockinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IflClockinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IflClockinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
