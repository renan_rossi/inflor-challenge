import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WorkersService } from '../../../core/services/workers.service';
import { Router } from '@angular/router';
import { Employee } from '../../models/entities/employee';

@Component({
  selector: 'ifl-employee-administration',
  templateUrl: './ifl-employee-administration.component.html',
  styleUrls: ['./ifl-employee-administration.component.scss']
})
export class IflEmployeeAdministrationComponent implements OnInit {

  constructor(private workersService: WorkersService) { }

  workers: Array<Employee>

  ngOnInit() {
    this.listWorkers()
  }

  insertWorker() {
    let worker = new Employee("Teste", new Date('12/03/2019'));

    worker.name = "Teste"
    worker.id = 1
    worker.occupation = "Desenvolvedor"


    this.workersService.insert(worker)
  }

  deleteWorker(worker: Employee){
    this.workersService.delete(worker.id)

    this.listWorkers()
  }

  async listWorkers() {
    let workers = await this.workersService.list()
    this.workers = workers
  }

}
