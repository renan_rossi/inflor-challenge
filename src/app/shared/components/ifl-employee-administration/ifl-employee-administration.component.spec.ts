import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IflEmployeeAdministrationComponent } from './ifl-employee-administration.component';

describe('IflEmployeeAdministrationComponent', () => {
  let component: IflEmployeeAdministrationComponent;
  let fixture: ComponentFixture<IflEmployeeAdministrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IflEmployeeAdministrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IflEmployeeAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
