import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WorkersService } from 'src/app/core/services/workers.service';
import { Router } from '@angular/router';
import { Employee } from '../../models/entities/employee';
import * as moment from 'moment';

@Component({
  selector: 'ifl-employee-register',
  templateUrl: './ifl-employee-register.component.html',
  styleUrls: ['./ifl-employee-register.component.scss']
})
export class IflEmployeeRegisterComponent implements OnInit {

  private employeeRegisterForm: FormGroup
  private _employeeRegisterForm(controlName: string) {
    return this.employeeRegisterForm.controls[controlName].value
  }

  constructor(private formBuilder: FormBuilder, private workersService: WorkersService, private router: Router) { }

  ngOnInit() {
    this.employeeRegisterForm = this.formBuilder.group({
      name: ['', Validators.required],
      occupation: ['', [Validators.required]],
      workingHours: [0, [Validators.required]],
      admission: [Date.now ,[Validators.required]]
    })
  }

  private submitWorkerForm() {
    let worker = new Employee(
      this._employeeRegisterForm('name'),
      moment(this._employeeRegisterForm('admission')).toDate()
    )
    worker.occupation = this._employeeRegisterForm('occupation')
    worker.workingHours = this._employeeRegisterForm('workingHours')

    this.workersService.insert(worker)

    this.employeeRegisterForm.reset()

    this.router.navigate(['ponto/colaboradores'])
  }
}
