import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IflEmployeeRegisterComponent } from './ifl-employee-register.component';

describe('IflEmployeeRegisterComponent', () => {
  let component: IflEmployeeRegisterComponent;
  let fixture: ComponentFixture<IflEmployeeRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IflEmployeeRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IflEmployeeRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
